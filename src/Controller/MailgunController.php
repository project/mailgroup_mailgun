<?php

namespace Drupal\mailgroup_mailgun\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller for Mail group Mailgun routes.
 */
class MailgunController extends ControllerBase {

  use StringTranslationTrait;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return (new static())
      ->setLogger($container->get('logger.channel.mailgroup_mailgun'));
  }

  /**
   * Set the logger.
   *
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   The logger channel.
   *
   * @return $this
   */
  protected function setLogger(LoggerChannelInterface $logger) {
    $this->logger = $logger;
    return $this;
  }

  /**
   * Responds to POST requests.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   An empty response.
   */
  public function receive(Request $request) {
    /** @var \Drupal\mailgroup\Entity\Storage\MailGroupStorageInterface $group_storage */
    $group_storage = $this->entityTypeManager()->getStorage('mailgroup');
    /** @var \Drupal\mailgroup\Entity\Storage\MailGroupMembershipStorageInterface $membership_storage */
    $membership_storage = $this->entityTypeManager()->getStorage('mailgroup_membership');
    $message_storage = $this->entityTypeManager()->getStorage('mailgroup_message');

    $recipient = $request->request->get('recipient');
    $group = $group_storage->loadByEmail($recipient);

    $from_email = $request->request->get('sender');
    $memberships = $membership_storage->loadByEmail($from_email, $group);
    $sending_member = reset($memberships);

    if ($group) {
      $message = $message_storage->create([
        'gid' => $group->id(),
        'uid' => $sending_member->getOwnerId(),
        'subject' => $request->request->get('subject'),
        'body' => $request->request->get('stripped-text'),
      ]);

      try {
        $message->save();
      }
      catch (EntityStorageException $e) {
        $this->logger->error($e->getMessage());
        $this->logger->error($this->t('Unable to save message to @group', [
          '@group' => $group->getName(),
        ]));
      }
    }

    return new Response();
  }

}
