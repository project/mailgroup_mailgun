<?php

namespace Drupal\mailgroup_mailgun\Plugin\Mailgroup\Connection;

use Drupal\mailgroup\ConnectionBase;
use Mailgun\Exception\HttpClientException;
use Mailgun\Mailgun as MailgunClient;
use Mailgun\Model\Domain\IndexResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Basic server connection plugin.
 *
 * @Connection(
 *   id = "mailgun",
 *   label = @Translation("Mailgun")
 * )
 */
class Mailgun extends ConnectionBase {

  /**
   * The Mailgun client.
   *
   * @var \Mailgun\Mailgun
   */
  protected $mailgun;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return (new static($configuration, $plugin_id, $plugin_definition))
      ->setMailgun($container->get('mailgun.mailgun_client'));
  }

  /**
   * Set the Mailgun client.
   *
   * @param \Mailgun\Mailgun $mailgun
   *   The Mailgun client.
   *
   * @return $this
   */
  protected function setMailgun(MailgunClient $mailgun) {
    $this->mailgun = $mailgun;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function testConnection() {
    try {
      $response = $this->mailgun
        ->domains()
        ->index();

      return $response instanceof IndexResponse;
    }
    catch (HttpClientException $e) {
      return FALSE;
    }
  }

}
